FROM node:16.14.2-alpine
WORKDIR /app

COPY package.json package-lock.json ./

RUN apk update && apk upgrade

RUN npm install

COPY . .

EXPOSE 1111
ENV NUXT_HOST=0.0.0.0

CMD ["npm", "start"]
